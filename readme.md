# gemhltv
![the gemhltv match page](matches.png) ![the gemhltv news page](news.png)

a gemini mirror of hltv.org

## requirements
- python3
- pytz
- php
- an instance of reader.php somewhere

## setup
- clone this repo
- cd to this repo
- `pip3 install pytz`
- put all the scripts in your cgi-bin
- put the logos folder in `/var/gemini-data`

## config
- change `LOGO_FILE` at the top of all the scripts if the logos folder is
  somewhere other than `/var/gemini-data/logos`
- change `CGI_BIN` at the top of `gemhltv` if the scripts are
  somewhere other than `/var/gemini/cgi-bin`
- change `READER_LINK` at the top of gemhltv\_news to point to your
  instance of reader.php
- change the current\_tz at the top of the gemhltv\_matches file to match
  yours
    - a list of valid timezones is available [here](https://gist.githubusercontent.com/heyalexej/8bf688fd67d7199be4a1682b3eec7568/raw/daacf0e4496ccc60a36e493f0252b7988bceb143/pytz-time-zones.py)
- you can edit the files in the `logos` folder to change the logos displayed at
  the top of each page/section



## notes
- rip c9

(c) oneseveneight/sose
